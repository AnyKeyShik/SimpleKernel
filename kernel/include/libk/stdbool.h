// Copyright (c) 2022. AnyKeyShik Rarity
//
// nikitav59@gmail.com
//
// https://t.me/AnyKeyShik

#ifndef SIMPLEKERNEL_STDBOOL_H
#define SIMPLEKERNEL_STDBOOL_H

typedef enum {
    false = 0,
    true = 1
} bool;

#define	false	false
#define	true	true

#define __bool_true_false_are_defined

#endif //SIMPLEKERNEL_STDBOOL_H
